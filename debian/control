Source: eclipse-tracecompass
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Sudip Mukherjee <sudipm.mukherjee@gmail.com>
Build-Depends: debhelper-compat (= 13), default-jdk,
 maven-debian-helper, libantlr3-runtime-java,
 libcommons-cli-java, libcommons-compress-java,
 libcommons-io-java, libcommons-jxpath-java,
 libcommons-lang3-java, libcommons-logging-java,
 libeclipse-cdt-java, libeclipse-debug-ui-java,
 libeclipse-emf-ecore-change-java, libeclipse-jdt-annotation-java,
 libeclipse-jsch-ui-java, libeclipse-linuxtools-java,
 libeclipse-remote-ui-java, libeclipse-search-java,
 libeclipse-ui-ide-application-java, libeclipse-ui-navigator-resources-java,
 libeclipse-ui-themes-java, libeclipse-wst-validation-ui-java,
 libeclipse-wst-xml-ui-java, libeclipse-wst-xsd-core-java,
 libeclipse-xsd-java, libequinox-event-java,
 libequinox-p2-ui-sdk-java, libfelix-scr-java,
 libgoogle-gson-java, libguava-java,
 libjaxb-api-java, libmaven-bundle-plugin-java,
 libopenjson-java, libsac-java,
 libswtchart-java, libmaven-antrun-plugin-java,
 libeclipse-swtchart-java
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/java-team/eclipse-tracecompass.git
Vcs-Browser: https://salsa.debian.org/java-team/eclipse-tracecompass
Homepage: https://www.eclipse.org/tracecompass/

Package: eclipse-tracecompass
Architecture: all
Depends: ${misc:Depends}, ${maven:Depends},
 libequinox-launcher-java, libswt-cairo-gtk-4-jni,
 libeclipse-osgi-compatibility-state-java, libeclipse-linuxtools-java,
 libeclipse-remote-ui-java, libeclipse-swtchart-java,
 libeclipse-wst-validation-ui-java, libeclipse-wst-xml-ui-java,
 libeclipse-wst-xsd-core-java, libeclipse-xsd-java,
 libfelix-scr-java, libantlr3-runtime-java,
 libcommons-beanutils-java, libcommons-cli-java,
 libcommons-compress-java, libcommons-io-java,
 libcommons-jxpath-java, libcommons-lang3-java,
 libeclipse-cdt-java, libeclipse-compare-core-java,
 libeclipse-debug-ui-java, libeclipse-e4-ui-bindings-java,
 libeclipse-e4-ui-css-swt-theme-java, libeclipse-e4-ui-ide-java,
 libeclipse-e4-ui-workbench-addons-swt-java, libeclipse-emf-ecore-change-java,
 libeclipse-emf-ecore-xmi-java, libeclipse-jface-databinding-java,
 libeclipse-jface-notifications-java, libeclipse-jsch-ui-java,
 libeclipse-osgi-util-java, libeclipse-search-java,
 libeclipse-ui-ide-application-java, libeclipse-ui-navigator-resources-java,
 libeclipse-ui-themes-java, libel-api-java,
 libequinox-bidi-java, libequinox-event-java,
 libequinox-p2-ui-sdk-java, libgeronimo-annotation-1.3-spec-java,
 libgoogle-gson-java, libguava-java,
 libjaxb-api-java, libjdom1-java,
 libjna-platform-java, libjsp-api-java,
 libopenjson-java, libsac-java,
 libservlet-api-java, libswtchart-java,
 libxerces2-java
Suggests: ${maven:OptionalDepends}
Description: Tool for viewing and analyzing logs and traces
 Eclipse Trace Compass is an open source application to solve performance
 and reliability issues by reading and analyzing traces and logs of a
 system. Its goal is to provide views, graphs, metrics, and more to help
 extract useful information from traces, in a way that is more
 user-friendly and informative than huge text dumps.
 .
 Supports Linux LTTng kernel traces with built-in analyses and views.
